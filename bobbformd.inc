<?php // (C) Copyright Bobbing Wide 2011
// bobbformd.inc
// bwdf = Bobbing Wide Drupal Form
// fs = FieldSet

function bwdf_text( &$form, $field, $title, $desc, $size, $maxlength, $required=FALSE, $validation_func=NULL ) {

  $form[$field] = array(
    '#type' => 'textfield',
    '#title' => t( $title),
    '#size' => $size,
    '#maxlength' => $maxlength,
    '#description' => t( $desc ),
    '#default_value' => variable_get( $field, '' ),
  );
  if ( $required )
    $form[$field]['#required'] = $required;
  
  if ( $validation_func )
    $form[$field]['#element_validate'] = array( $validation_func );
}


function bwdf_fieldset( &$form, $fieldset, $title, $desc, $collapsible=TRUE, $collapsed=FALSE ) {
  $form[$fieldset] = array( 
    '#title' => t( $title),
    '#type' => 'fieldset',
    '#description' => t( $desc ),
    '#collapsible' => $collapsible,
    '#collapsed' => $collapsed,
  );  
}


function bwdf_fs_text( &$form, $fieldset, $field, $title, $desc, $size, $maxlength, $required=FALSE, $validation_func=NULL ) {

  $form[$fieldset][$field] = array(
    '#type' => 'textfield',
    '#title' => t( $title),
    '#size' => $size,
    '#maxlength' => $maxlength,
    '#description' => t( $desc ),
    '#default_value' => variable_get( $field, '' ),
  );
  if ( $required )
    $form[$fieldset][$field]['#required'] = $required;
  
  if ( $validation_func )
    $form[$fieldset][$field]['#element_validate'] = array( $validation_func );
}


function bwdf_area( &$form, $field, $title, $desc, $cols, $rows, $required=FALSE, $validation_func=NULL ) {

  $form[$field] = array(
    '#type' => 'textarea',
    '#title' => t( $title),
    '#cols' => $cols,
    '#rows' => $rows,
    '#description' => t( $desc ),
  );
  
  if ( $required )
    $form[$field]['#required'] = $required;
  
  if ( $validation_func )
    $form[$field]['#element_validate'] = array( $validation_func );
} 

function bwdf_select( &$form, $field, $title, $desc, $options ) {
  $form[$field] = array(
    '#type' => 'select',
    '#title' => t( $title),
    '#description' => t( $desc ),
    '#options' => $options
  );

} 

function bwdf_multi_select( &$form, $field, $title, $desc, $options ) {
  bwdf_select( &$form, $field, $title, $desc, $options );
  $form[$field]['#multiple'] = TRUE;
  $form[$field]['#size'] = min(20, count($options));
  
}




function bwdf_email_validate( $element, &$form_state) {
  $email = $form_state['values']['email'];
  if ( $email == "herb" )
    form_set_error( 'email', 'email can\'t be herb');
  //else
    // form_set_error( 'email', 'email is:' . $email.'!' );                                    
  
}


function bwdf_fs_select( &$form, $fieldset, $field, $title, $desc, $options, $required=TRUE ) {
  $form[$fieldset][$field] = array(
    '#type' => 'select',
    '#title' => t( $title),
    '#description' => t( $desc ),
    '#options' => $options,
    '#required' => $required,
  );

}

 
function bwdf_fs_multi_select( &$form, $fieldset, $field, $title, $desc, $options,$required=FALSE ) {
  bwdf_fs_select( &$form, $fieldset, $field, $title, $desc, $options, $required );
  $form[$fieldset][$field]['#multiple'] = TRUE;
  $form[$fieldset][$field]['#size'] = min(20, count($options));
}


