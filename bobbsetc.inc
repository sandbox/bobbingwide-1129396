<?php 

die( "This code in bobbsetc.inc is no longer needed for WordPress or Drupal installations" );

function bw_set_company( $field, $value=NULL ) 
{
   global $company;
   $company[ $field ] = $value; 
}   

function bw_company_tel( $telephone = NULL, $fax = NULL, $mobile= NULL )   
{

   bw_set_company( "telephone", $telephone );
   bw_set_company( "fax", $fax );
   bw_set_company( "mobile", $mobile );
}

function bw_company_job( $company = NULL, $business = NULL, $formal = NULL )   
{
   //global $header_title, $header_subtitle;

   bw_set_company( "company", $company );
   bw_set_company( "business", $business );
   bw_set_company( "formal", $formal );
   
   //$header_title = bw_get_company( "company" );
   //$header_subtitle = bw_get_company( "business" );
     
}

function bw_company_slogans( $main_slogan                     // "main-slogan" 
                           , $alt_slogan                      // "alt-slogan "
                           )
{
   bw_set_company( "main-slogan", $main_slogan );
   bw_set_company( "alt-slogan", $alt_slogan );
}

function bw_company_contacts( $contact = NULL, $email = NULL, $admin= NULL )
{
   bw_set_company( "contact", $contact );
   bw_set_company( "email", $email );
   bw_set_company( "admin", $admin ); 
}

                                   
// extended-address e.g.  1 Fernwood House
// street-address   e.g.  45-47 London Road                            
// locality         e.g   Cowplain
// region           e.g.  WATERLOOVILLE, HANTS                         
// postal-code      e.g.  PO8 8H                        
// country-name     e.g.  UK                          


function bw_company_address( $extended_address=NULL, $street_address=NULL, $locality=NULL, $region=NULL, $postal_code=NULL, $country_name=NULL )
{
  bw_set_company( "extended-address", $extended_address );
  bw_set_company( "street-address", $street_address  );
  bw_set_company( "locality", $locality  );
  bw_set_company( "region", $region );
  bw_set_company( "postal-code", $postal_code );   
  bw_set_company( "country-name", $country_name );

}

function bw_company_geo( $lat, $long, $google_api_key, $betterbyfar="betterbyfar" )
{
  bw_set_company( "lat", $lat );
  bw_set_company( "long", $long );
  bw_set_company( "google_maps_api_key", $google_api_key );
  bw_set_company( "betterbyfar", $betterbyfar );
}


function bw_set_servers( $domain, $parked )
{
  bw_set_company( "domain", $domain );
  bw_set_company( "parked", $parked );
}   
  
function bw_set_verification( $domain, $google, $yahoo, $bing )
{
  // we pass domain in case we need to set domain specific values - save this as vdomain rather than overwrite domain
  bw_set_company( "vdomain", $domain );
  bw_set_company( "google", $google );
  bw_set_company( "yahoo", $yahoo );
  bw_set_company( "bing", $bing );
}   
  

